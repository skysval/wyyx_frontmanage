package com.kgc.cn.provider.serviceImpl;

import com.alibaba.dubbo.config.annotation.Service;
import com.google.common.collect.Lists;
import com.kgc.cn.common.model.Order;
import com.kgc.cn.common.model.OrderAndGoods;
import com.kgc.cn.common.service.OrderService;
import com.kgc.cn.common.service.ShopCartService;
import com.kgc.cn.common.utils.id.IdUtils;
import com.kgc.cn.common.vo.GoodsQueryVo;
import com.kgc.cn.provider.mapper.GoodsMapper;
import com.kgc.cn.provider.mapper.OrderAndGoodsMapper;
import com.kgc.cn.provider.mapper.OrderMapper;
import com.kgc.cn.provider.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private GoodsMapper goodsMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private OrderAndGoodsMapper orderAndGoodsMapper;

    @Autowired
    private ShopCartService shopCartService;


    /**
     * 单个商品生成订单
     *
     * @param uid
     * @param gid
     * @param count
     */
    @Override
    @Transactional
    public Order queryOrder(String uid, String gid, int count) {
        long price = goodsMapper.selPriceByGid(gid);
        String phone = userMapper.selPhoneById(uid);
        String oid = IdUtils.queryOid();
        long realPrice;
        GoodsQueryVo goodsQueryVo = goodsMapper.queryGoodsById(gid).toGoodsQueryVo();
        if (goodsQueryVo.getDiscountVo() == null) {
            realPrice = goodsQueryVo.getgPrice() * count;
        } else {
            realPrice = goodsQueryVo.getDiscountVo().getDiscountPrice() * count;
        }
        Order order = new Order(oid, uid, realPrice, 0);
        orderMapper.insertSelective(order);
        OrderAndGoods orderAndGoods = new OrderAndGoods(oid, gid, count);
        orderAndGoodsMapper.insertSelective(orderAndGoods);
        return order;
    }

    /**
     * 异步生成订单
     *
     * @param uid
     * @param countMap
     * @param oid
     * @param sumPrice
     */
    @Transactional
    @Async
    public void queryOrderList(String uid, Map<String, Integer> countMap, String oid, long sumPrice) {
        List<String> list = Lists.newArrayList();
        countMap.forEach((gid, count) -> {
            list.add(gid);
            OrderAndGoods orderAndGoods = new OrderAndGoods(oid, gid, count);
            orderAndGoodsMapper.insertSelective(orderAndGoods);
        });
        Order order = new Order(oid, uid, sumPrice, 0);
        orderMapper.insertSelective(order);
        shopCartService.cleanShopCart(uid, list);

    }

    @Override
    public Order queryOrderById(String oid) {
        Order order = orderMapper.selectByPrimaryKey(oid);
        return order;
    }

    @Override
    public Order rushOrder(String uid, String gid) {
        String phone = userMapper.selPhoneById(uid);
        String oid = IdUtils.queryOid();
        long realPrice;
        GoodsQueryVo goodsQueryVo = goodsMapper.queryGoodsById(gid).toGoodsQueryVo();
        if (goodsQueryVo.getDiscountVo() == null) {
            realPrice = goodsQueryVo.getgPrice();
        } else {
            realPrice = goodsQueryVo.getDiscountVo().getDiscountPrice();
        }
        Order order = new Order(oid, uid, realPrice, 0);
        return order;
    }

    @Override
    public int addRushOrder(Order order, String gid) {
        orderMapper.insertSelective(order);
        OrderAndGoods orderAndGoods = new OrderAndGoods(order.getoId(), gid, 1);
        orderAndGoodsMapper.insertSelective(orderAndGoods);
        return 1;
    }


}
