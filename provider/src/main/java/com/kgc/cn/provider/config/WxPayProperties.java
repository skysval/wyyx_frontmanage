package com.kgc.cn.provider.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "wxPay")
public class WxPayProperties {

    private String appId;
    private String machId;
    private String notifyUrl;
    private String key;
    private String url;
}
