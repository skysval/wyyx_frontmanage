package com.kgc.cn.provider.serviceImpl;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.dubbo.config.annotation.Service;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.kgc.cn.common.model.*;
import com.kgc.cn.common.service.OrderService;
import com.kgc.cn.common.service.UserService;
import com.kgc.cn.common.utils.id.IdUtils;
import com.kgc.cn.common.vo.GoodsQueryVo;
import com.kgc.cn.common.vo.UserLoginVo;
import com.kgc.cn.common.vo.UserResignVo;
import com.kgc.cn.provider.mapper.GoodsMapper;
import com.kgc.cn.provider.mapper.ShopCartMapper;
import com.kgc.cn.provider.mapper.UserMapper;
import com.kgc.cn.provider.mapper.WxUserMapper;
import com.kgc.cn.provider.utils.aes.AesUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private WxUserMapper wxUserMapper;

    @Autowired
    private GoodsMapper goodsMapper;

    @Autowired
    private ShopCartMapper shopCartMapper;

    @Autowired
    private OrderService orderService;

    @Override
    public User checkWxLogin(String openId) {
        if (ObjectUtils.isEmpty(wxUserMapper.selectByPrimaryKey(openId))) {
            wxUserMapper.insertSelective(WxUser.builder().openId(openId).build());
            return null;
        }
        WxUser wxUser = wxUserMapper.selectByPrimaryKey(openId);
        if (StringUtils.isNotEmpty(wxUser.getPhone())) {
            User user = queryUserByPhone(wxUser.getPhone());
            user.setuPassword(null);
            return user;
        }
        return null;
    }

    @Override
    public User queryUserByPhone(String phone) {
        UserExample userExample = new UserExample();
        userExample.createCriteria().andUPhoneEqualTo(phone);
        List<User> userList = userMapper.selectByExample(userExample);
        if (CollectionUtils.isNotEmpty(userList)) {
            User user = userList.get(0);
            user.setuPassword(null);
            return user;
        }
        return null;
    }

    @Override
    @Transactional
    public User insertWxUser(String phone, String nickName, String openId) {
        WxUser wxUser = WxUser.builder().openId(openId).phone(phone).build();
        int flag = wxUserMapper.updateByPrimaryKeySelective(wxUser);
        if (flag == 1) {
            User user = new User(IdUtils.getUId(phone), nickName, phone);
            if (userMapper.insertSelective(user) == 1) {
                return user;
            }
        }
        return null;
    }

    @Override
    public User login(UserLoginVo userLoginVo) throws Exception {
        UserExample userExample = new UserExample();
        userExample.createCriteria().andUPhoneEqualTo(userLoginVo.getuPhone());
        List<User> users = userMapper.selectByExample(userExample);
        if (CollectionUtils.isNotEmpty(users)) {
            User user = users.get(0);
            if (StringUtils.isNotEmpty(user.getuPassword()) &&
                    AesUtils.decrypt(user.getuPassword()).equals(userLoginVo.getuPassword())) {
                user.setuPassword(AesUtils.decrypt(user.getuPassword()));
                user.setuPassword(null);
                return user;
            }
        }
        return null;
    }

    @Override
    public User register(UserResignVo userResignVo) throws Exception {
        User user = userResignVo.toUser();
        user.setuId(IdUtils.getUId(userResignVo.getuPhone()));
        UserExample userExample = new UserExample();
        userExample.createCriteria().andUPasswordEqualTo(user.getuPhone());
        long flag = userMapper.countByExample(userExample);
        if (flag == 0) {
            user.setuPassword(AesUtils.encrypt(user.getuPassword()));
            userMapper.insertSelective(user);
            user.setuPassword(null);
            return user;
        }
        return null;
    }


    public Map<String, Integer> selCount(String gid, String uid) {
        String[] gids = gid.split(",");
        List<String> lists = Arrays.asList(gids);
        Map<String, Integer> countMap = Maps.newHashMap();
        lists.forEach(goodid -> {
            int count = shopCartMapper.selCouFromCart(goodid, uid);//数量
            countMap.put(goodid, count);
        });
        return countMap;
    }


    /**
     * 购物车生成订单
     *
     * @param uid
     * @param countMap
     */
    @Override
    @Transactional(readOnly = true)
    public Order queryOrders(String uid, Map<String, Integer> countMap, String oid) {

        List<GoodsQueryVo> goodsVo = Lists.newArrayList();
        countMap.keySet().forEach(eachgood -> {
            Goods goods = goodsMapper.queryGoodsById(eachgood);
            GoodsQueryVo goodsQueryVo = goods.toGoodsQueryVo();
            goodsVo.add(goodsQueryVo);
        });
        long sumPrice = goodsVo.stream().mapToLong(goodsQueryVo -> {
            long count = countMap.get(goodsQueryVo.getgId());
            if (goodsQueryVo.getDiscountVo() != null) {
                return goodsQueryVo.getDiscountVo().getDiscountPrice() * count;
            } else {
                return goodsQueryVo.getgPrice() * count;
            }
        }).sum();
        Order order = new Order(oid, sumPrice);
        orderService.queryOrderList(uid, countMap, oid, sumPrice);
        return order;
    }

    @Override
    public User queryUserById(String uid) {
        User user = userMapper.selectByPrimaryKey(uid);
        user.setuPassword(null);
        return user;
    }
}
