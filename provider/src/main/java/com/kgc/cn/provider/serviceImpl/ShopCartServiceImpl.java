package com.kgc.cn.provider.serviceImpl;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.alibaba.dubbo.config.annotation.Service;
import com.google.common.collect.Lists;
import com.kgc.cn.common.model.*;
import com.kgc.cn.common.service.ShopCartService;
import com.kgc.cn.common.vo.Page;
import com.kgc.cn.common.vo.ShopCartShowVo;
import com.kgc.cn.common.vo.ShopCartVo;
import com.kgc.cn.provider.mapper.GoodsMapper;
import com.kgc.cn.provider.mapper.ShopCartMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
@SuppressWarnings("all")
public class ShopCartServiceImpl implements ShopCartService {

    @Autowired
    private ShopCartMapper shopCartMapper;

    @Autowired
    private GoodsMapper goodsMapper;


    /**
     * 未登录状态添加购物车
     *
     * @param dId
     * @param goodsNum
     * @return
     */
    @Override
    public int addCartOut(ShopCartVo shopCartVo) {
        Goods goods = goodsMapper.selectByPrimaryKey(shopCartVo.getgId());
        if (null != goods) {
            if (goods.getgStock() < shopCartVo.getgCount()) {
                return 2;
            }
            return 1;
        }
        return 0;
    }

    @Override
    public int addCartIn(ShopCartVo shopCartVo, User user) {
        Goods goods = goodsMapper.selectByPrimaryKey(shopCartVo.getgId());
        if (null != goods) {
            if (goods.getgStock() < shopCartVo.getgCount()) {
                return 2;
            }
            ShopCart shopCart = ShopCart.builder().gId(shopCartVo.getgId()).uId(user.getuId()).gCount(shopCartVo.getgCount()).build();
            ShopCartExample shopCartExample = new ShopCartExample();
            shopCartExample.createCriteria().andGIdEqualTo(shopCartVo.getgId()).andUIdEqualTo(user.getuId());
            List<ShopCart> shopCartList = shopCartMapper.selectByExample(shopCartExample);
            if (CollectionUtils.isNotEmpty(shopCartList)) {
                int num = shopCartList.get(0).getgCount();
                shopCart = ShopCart.builder().gId(shopCartVo.getgId()).uId(user.getuId()).gCount(shopCartVo.getgCount() + num).build();
                shopCartMapper.updateByExample(shopCart, shopCartExample);
            } else {
                shopCartMapper.insert(shopCart);
            }
            return 1;
        }
        return 0;
    }

    @Override
    public int delCart(List<String> gIdList) {
        gIdList.forEach(gId -> {
            ShopCartExample shopCartExample = new ShopCartExample();
            shopCartExample.createCriteria().andGIdEqualTo(gId);
            shopCartMapper.deleteByExample(shopCartExample);
        });
        return 1;
    }

    @Override
    public int addShopCartLogin(ShopCart shopCart) {
        if (shopCartMapper.insert(shopCart) > 0) {
            return 1;
        }
        return 0;
    }

    @Override
    public Page showCart(int pageSize, int pageNo, User user) {
        ShopCartExample shopCartExample = new ShopCartExample();
        shopCartExample.createCriteria().andUIdEqualTo(user.getuId());
        shopCartExample.setPageSize(pageSize);
        shopCartExample.setStartRow((pageNo - 1) * pageSize);
        List<ShopCart> shopCartList = shopCartMapper.selectByExample(shopCartExample);
        List<ShopCartShowVo> shopCartShowVosList = Lists.newArrayList();
        shopCartList.forEach(cart -> {
            Goods goods = goodsMapper.showGoods(cart.getgId());
            ShopCartShowVo shopCartShowVo = ShopCartShowVo.builder().goods(goods).gCount(cart.getgCount()).build();
            shopCartShowVosList.add(shopCartShowVo);
        });
        int totalNum = shopCartList.size();
        Page page = new Page();
        page.setPage(totalNum, pageSize, pageNo);
        page.setList(shopCartShowVosList);
        return page;
    }

    @Override
    public Page showCartOut(int pageSize, int pageNo, List<ShopCartVo> shopCartVoList) {
        List<ShopCartShowVo> shopCartShowVosList = Lists.newArrayList();
        int ps = (pageNo - 1) * pageSize + pageSize;
        if (ps > shopCartVoList.size()) ps = shopCartVoList.size();
        for (int i = (pageNo - 1) * pageSize; i < ps; i++) {
            GoodsExample goodsExample = new GoodsExample();
            Goods goods = goodsMapper.showGoods(shopCartVoList.get(i).getgId());
            ShopCartShowVo shopCartShowVo = ShopCartShowVo.builder().goods(goods).gCount(shopCartVoList.get(i).getgCount()).build();
            shopCartShowVosList.add(shopCartShowVo);
        }
        int totalNum = shopCartShowVosList.size();
        Page page = new Page();
        page.setPage(totalNum, pageSize, pageNo);
        page.setList(shopCartShowVosList);
        return page;
    }

    @Override
    public void cleanShopCart(String uid, List<String> list) {
        ShopCartExample shopCartExample = new ShopCartExample();
        shopCartExample.createCriteria().andUIdEqualTo(uid).andGIdIn(list);
        shopCartMapper.deleteByExample(shopCartExample);
    }


}
