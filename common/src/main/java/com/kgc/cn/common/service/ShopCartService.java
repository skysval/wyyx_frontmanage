package com.kgc.cn.common.service;

import com.kgc.cn.common.model.ShopCart;
import com.kgc.cn.common.model.User;
import com.kgc.cn.common.vo.Page;
import com.kgc.cn.common.vo.ShopCartVo;

import java.util.List;

public interface ShopCartService {
    /**
     * 未登录状态添加购物车
     *
     * @param shopCartVo
     * @return
     */
    int addCartOut(ShopCartVo shopCartVo);


    /**
     * 登录状态添加购物车
     *
     * @param shopCartVo
     * @return
     */
    int addCartIn(ShopCartVo shopCartVo, User user);


    /**
     * 删除购物车
     *
     * @param gIdList
     * @return
     */
    int delCart(List<String> gIdList);


    /**
     * 登录时读取redis中购物车放入数据库中
     *
     * @param shopCart
     * @return
     */
    int addShopCartLogin(ShopCart shopCart);


    /**
     * 显示用户购物车
     *
     * @param pageSize
     * @param pageNo
     * @param user
     * @return
     */
    Page showCart(int pageSize, int pageNo, User user);

    /**
     * 未登录时展示购物车
     *
     * @param pageSize
     * @param pageNo
     * @param shopCartVoList
     * @return
     */
    Page showCartOut(int pageSize, int pageNo, List<ShopCartVo> shopCartVoList);

    void cleanShopCart(String uid,List<String> list);
}
