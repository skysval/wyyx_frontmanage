package com.kgc.cn.common.enums;

import lombok.Getter;

@Getter
public enum RedisEnums {
    STOCK_NAME_SPACE("stock:"),
    WAIT_NAME_SPACE("wait"),
    REDIS_LOCK("lock"),
    ORDER_RUSH("rushOrder:"),
    ORDER_COMMON("commonOrder:");

    public static final String STOCK_NAMESPACE = "stock:";
    public static final String RUSH_ORDER = "rushOrder:";

    private String name;


    RedisEnums(String name) {
        this.name = name;
    }
}
