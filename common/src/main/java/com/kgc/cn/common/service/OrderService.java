package com.kgc.cn.common.service;

import com.kgc.cn.common.model.Order;

import java.util.Map;

public interface OrderService {

    Order queryOrder(String uid, String gid, int count);

    void queryOrderList(String uid, Map<String, Integer> countMap, String oid, long sumPrice);

    Order queryOrderById(String oid);

    /**
     * 生成抢购订单
     *
     * @param uid
     * @param gid
     * @return
     */
    Order rushOrder(String uid, String gid);

    /**
     * 抢购订单入库
     *
     * @param order
     * @return
     */
    int addRushOrder(Order order, String gid);

}
