package com.kgc.cn.common.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

@ApiModel(value = "分页")
public class Page implements Serializable {

    @ApiModelProperty(value = "页面条数")
    private int pageSize;

    @ApiModelProperty(value = "总条数")
    private int totalNum;

    @ApiModelProperty(value = "总页数")
    private int totalSize;

    @ApiModelProperty(value = "当前页")
    private int currentPage;

    @ApiModelProperty(value = "下一页")
    private int afterPage;

    @ApiModelProperty(value = "上一页")
    private int beforePage;

    @ApiModelProperty(value = "首页")
    private int firstPage;

    @ApiModelProperty(value = "末页")
    private int lastPage;


    private List list;

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(int totalNum) {
        this.totalNum = totalNum;
    }

    public int getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(int totalSize) {
        this.totalSize = totalSize;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getAfterPage() {
        return afterPage;
    }

    public void setAfterPage(int afterPage) {
        this.afterPage = afterPage;
    }

    public int getBeforePage() {
        return beforePage;
    }

    public void setBeforePage(int beforePage) {
        this.beforePage = beforePage;
    }

    public int getFirstPage() {
        return firstPage;
    }

    public void setFirstPage(int firstPage) {
        this.firstPage = firstPage;
    }

    public int getLastPage() {
        return lastPage;
    }

    public void setLastPage(int lastPage) {
        this.lastPage = lastPage;
    }


    public void setPage(int totalNum, int pageSize, int currentPage) {
        this.totalSize = totalNum % pageSize > 0 ? totalNum / pageSize + 1 : totalNum / pageSize;
        this.firstPage = 1;
        this.lastPage = this.totalSize;
        if (currentPage > 1) {
            this.beforePage = currentPage - 1;
        } else {
            this.beforePage = currentPage;
        }
        if (currentPage < this.lastPage) {
            this.afterPage = currentPage + 1;
        } else {
            this.afterPage = currentPage;
        }
        this.totalNum = totalNum;
        this.pageSize = pageSize;
        this.currentPage = currentPage;
    }
}
