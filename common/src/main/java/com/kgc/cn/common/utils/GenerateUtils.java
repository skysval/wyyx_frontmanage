package com.kgc.cn.common.utils;

import java.util.UUID;

public class GenerateUtils {

    public static String generateUUID() {
        return UUID.randomUUID().toString().replace("-", "").substring(0, 32);
    }
}
