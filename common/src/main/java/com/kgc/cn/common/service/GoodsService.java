package com.kgc.cn.common.service;

import com.kgc.cn.common.model.Goods;
import com.kgc.cn.common.model.OrderAndGoods;
import com.kgc.cn.common.vo.GoodsQueryVo;
import com.kgc.cn.common.vo.Page;

import java.util.List;
import java.util.Map;

public interface GoodsService {
    /**
     * 展示商品详情
     */
    GoodsQueryVo showDetails(String gid);

    /**
     * 展示全部商品
     */
    Map<String, Map<String, List<GoodsQueryVo>>> showAll();

    /**
     * 展示所有商品库存
     *
     * @return
     */
    List<Goods> goodsNum();

    /**
     * 修改库存
     *
     * @param gid
     * @param count
     */
    void updateStock(String gid, Integer count);

    /**
     * 修改销量
     *
     * @param gid
     * @param count
     */
    void updateSales(String gid, Integer count);

    List<OrderAndGoods> getGoodsAndCounts(String oid);

    void updateStatus(String oid, int stu);

    /**
     * 返回从成交额
     *
     * @return
     */
    Long queryTotalSale();

    /**
     * 返回从成交量
     *
     * @return
     */
    Long queryTotalVolume();

    Page queryGoodsByProperty(String property, int startRow, int pageSize);
}
