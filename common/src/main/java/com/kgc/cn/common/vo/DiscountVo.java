package com.kgc.cn.common.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;

import java.io.Serializable;
import java.util.Date;

@Builder
public class DiscountVo implements Serializable {

    @ApiModelProperty(value = "打折开始时间")
    private Date dStartTime;

    @ApiModelProperty(value = "打折结束时间")
    private Date dEndTime;

    @ApiModelProperty(value = "折扣力度")
    private Integer dCount;

    @ApiModelProperty(value = "是否折扣")
    private Integer dStatus;

    @ApiModelProperty(value = "打折后价格，单位分")
    private Long discountPrice;

    public Date getdStartTime() {
        return dStartTime;
    }

    public void setdStartTime(Date dStartTime) {
        this.dStartTime = dStartTime;
    }

    public Date getdEndTime() {
        return dEndTime;
    }

    public void setdEndTime(Date dEndTime) {
        this.dEndTime = dEndTime;
    }

    public Integer getdCount() {
        return dCount;
    }

    public void setdCount(Integer dCount) {
        this.dCount = dCount;
    }

    public Integer getdStatus() {
        return dStatus;
    }

    public void setdStatus(Integer dStatus) {
        this.dStatus = dStatus;
    }

    public Long getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(Long discountPrice) {
        this.discountPrice = discountPrice;
    }
}
