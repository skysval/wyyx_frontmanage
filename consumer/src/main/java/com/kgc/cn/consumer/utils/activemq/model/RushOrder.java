package com.kgc.cn.consumer.utils.activemq.model;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class RushOrder implements Serializable {
    private String oId;
    private String uId;
    private Long oPrice;
    private String gid;
}
