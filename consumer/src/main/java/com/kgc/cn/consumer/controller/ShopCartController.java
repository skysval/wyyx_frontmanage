package com.kgc.cn.consumer.controller;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.kgc.cn.common.enums.Enums;
import com.kgc.cn.common.model.User;
import com.kgc.cn.common.service.ShopCartService;
import com.kgc.cn.common.utils.returns.ReturnResult;
import com.kgc.cn.common.utils.returns.ReturnResultUtils;
import com.kgc.cn.common.vo.Page;
import com.kgc.cn.common.vo.ShopCartVo;
import com.kgc.cn.consumer.config.aop.CurrentUser;
import com.kgc.cn.consumer.config.aop.UserLoginRequired;
import com.kgc.cn.consumer.utils.ip.IpUtils;
import com.kgc.cn.consumer.utils.redis.RedisUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@Api(tags = "购物车")
@RestController
@RequestMapping(value = "/shopCart")
public class ShopCartController {

    @Autowired
    private RedisUtils redisUtils;

    @Reference
    private ShopCartService shopCartService;


    @ApiOperation(value = "添加购物车（未登录）")
    @GetMapping(value = "/addShopCartOut")
    public ReturnResult addShopCartOut(@Valid ShopCartVo shopCartVo, HttpServletRequest request) {
        String key = getIp(request) + ":" + shopCartVo.getgId();
        int flag = shopCartService.addCartOut(shopCartVo);
        if (flag == 1) {
            if (redisUtils.hasKey(key)) {
                int num = (int) redisUtils.get(key);
                redisUtils.set(key, num + shopCartVo.getgCount(), 60 * 60 * 24);
            } else {
                redisUtils.set(key, shopCartVo.getgCount(), 60 * 60 * 24);
            }
            return ReturnResultUtils.returnSuccess(Enums.ShopCartEnum.ADD_CART_SUCCESS);
        } else if (flag == 2) {
            return ReturnResultUtils.returnFail(Enums.ShopCartEnum.COUNT_CART_FAIL);
        }
        return ReturnResultUtils.returnFail(Enums.ShopCartEnum.ADD_CART_FAIL);
    }


    @UserLoginRequired
    @ApiOperation(value = "添加购物车（已登录）")
    @GetMapping(value = "/addShopCartIn")
    public ReturnResult addShopCarIn(@Valid ShopCartVo shopCartVo, @CurrentUser User user) {
        int flag = shopCartService.addCartIn(shopCartVo, user);
        if (flag == 1) {
            return ReturnResultUtils.returnSuccess(Enums.ShopCartEnum.ADD_CART_SUCCESS);
        } else if (flag == 2) {
            return ReturnResultUtils.returnFail(Enums.ShopCartEnum.COUNT_CART_FAIL);
        }
        return ReturnResultUtils.returnFail(Enums.ShopCartEnum.ADD_CART_FAIL);
    }


    @Transactional
    @ApiOperation(value = "删除购物车")
    @GetMapping(value = "/delShopCart")
    public ReturnResult<String> delShopCart(@ApiParam(value = "商品id", required = true) @RequestParam List<String> gIdList, HttpServletRequest request) {
        User user = getUserByToken(request);
        if (null == user) {
            String ip = getIp(request);
            gIdList.forEach(gId -> {
                redisUtils.del(ip + ":" + gId);
            });
            return ReturnResultUtils.returnSuccess(Enums.ShopCartEnum.DEL_CART_SUCCESS);
        }
        if (shopCartService.delCart(gIdList) == 1) {
            return ReturnResultUtils.returnSuccess(Enums.ShopCartEnum.DEL_CART_SUCCESS);
        }
        return ReturnResultUtils.returnFail(Enums.ShopCartEnum.DEL_CART_FAIL);
    }


    @Transactional
    @ApiOperation(value = "展示购物车")
    @GetMapping(value = "/showShopCart")
    public ReturnResult<Page> show(@ApiParam(value = "每页展示数量") @RequestParam(defaultValue = "10") int pageSize,
                                   @ApiParam(value = "起始页数") @RequestParam(defaultValue = "1") int pageNo,
                                   HttpServletRequest request) {
        User user = getUserByToken(request);
        if (null == user) {
            String ip = IpUtils.getIpAddress(request);
            Set<String> keysSet = redisUtils.keys(ip + "*");
            List<ShopCartVo> shopCartVoList = Lists.newArrayList();
            keysSet.forEach(key -> {
                int goodsNum = (int) redisUtils.get(key);
                String gId = key.substring(key.indexOf(":") + 1);
                ShopCartVo shopCartVo = new ShopCartVo(gId, goodsNum);
                shopCartVoList.add(shopCartVo);
            });
            Page pageOut = shopCartService.showCartOut(pageSize, pageNo, shopCartVoList);
            if (CollectionUtils.isNotEmpty(pageOut.getList())) {
                return ReturnResultUtils.returnSuccess(pageOut);
            }
            return ReturnResultUtils.returnSuccess(Enums.ShopCartEnum.EMPTY_CART);
        }
        Page page = shopCartService.showCart(pageSize, pageNo, user);
        if (CollectionUtils.isNotEmpty(page.getList())) {
            return ReturnResultUtils.returnSuccess(page);
        }
        return ReturnResultUtils.returnSuccess(Enums.ShopCartEnum.EMPTY_CART);
    }


    private String getIp(HttpServletRequest request) {
        return IpUtils.getIpAddress(request);
    }

    private User getUserByToken(HttpServletRequest request) {
        String token = request.getHeader("user_token");
        String userJsonStr = (String) redisUtils.get(Enums.CommonEnum.TOKEN_NAME_SPACE + token);
        User user = JSONObject.parseObject(userJsonStr, User.class);
        return user;
    }


}
