package com.kgc.cn.consumer.controller;

import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.kgc.cn.common.enums.Enums;
import com.kgc.cn.common.model.ShopCart;
import com.kgc.cn.common.model.User;
import com.kgc.cn.common.service.ShopCartService;
import com.kgc.cn.common.service.UserService;
import com.kgc.cn.common.utils.returns.ReturnResult;
import com.kgc.cn.common.utils.returns.ReturnResultUtils;
import com.kgc.cn.common.vo.UserLoginVo;
import com.kgc.cn.common.vo.UserResignVo;
import com.kgc.cn.consumer.config.aop.CurrentUser;
import com.kgc.cn.consumer.config.aop.UserLoginRequired;
import com.kgc.cn.consumer.utils.Sms;
import com.kgc.cn.consumer.utils.UserUtils;
import com.kgc.cn.consumer.utils.ip.IpUtils;
import com.kgc.cn.consumer.utils.redis.RedisUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Set;

@RestController
@Api(tags = "用户操作")
@RequestMapping(value = "/user")
public class UserController {

    @Reference
    private UserService userService;

    @Reference
    private ShopCartService shopCartService;

    @Autowired
    private RedisUtils redisUtils;

    @ApiOperation(value = "发送手机验证码（绑定手机号用）")
    @PostMapping(value = "/sendCheckSms")
    public ReturnResult<String> sendCheckSms(@ApiParam(value = "手机号", required = true) @RequestParam String phone) {
        if (!ObjectUtils.isEmpty(userService.queryUserByPhone(phone))) {
            return ReturnResultUtils.returnFail(Enums.CommonEnum.PHONE_ALREADY_BIND);
        }
        // 60秒内只能发送一次
        if (redisUtils.checkSpeed(Enums.CommonEnum.CHECK_SPEED_NAME_SPACE + phone, 1, 60)) {
            // 随机生成6位验证码发送
            String code = Sms.getRandom().toString();
            redisUtils.set(Enums.CommonEnum.CODE_NAME_SPACE + phone, code, 30 * 60);
            Sms.sendSms(phone, "{code:" + code + "}", "SMS_180960940");
            return ReturnResultUtils.returnSuccess(Enums.CommonEnum.SEND_CODE_SUCCESS);
        }
        return ReturnResultUtils.returnFail(Enums.CommonEnum.CODE_SEND_ERROR);
    }

    @ApiOperation(value = "绑定手机号")
    @PostMapping(value = "/bindPhone")
    public ReturnResult<String> bindPhone(@ApiParam(value = "手机号", required = true) @RequestParam String phone,
                                          @ApiParam(value = "验证码", required = true) @RequestParam String code,
                                          HttpServletRequest request) {
        Object redisCode = redisUtils.get(Enums.CommonEnum.CODE_NAME_SPACE + phone);
        if (!ObjectUtils.isEmpty(redisCode) && StringUtils.isNotEmpty(code) && code.equals(redisCode.toString())) {
            String state = request.getHeader("state");
            String openId = redisUtils.get(Enums.CommonEnum.STATE_NAME_SPACE + state).toString();
            String nickName = UserUtils.getNickName(redisUtils.get(Enums.CommonEnum.OPEN_ID_NAME_SPACE + openId));
            User user = userService.insertWxUser(phone, nickName, openId);
            if (!ObjectUtils.isEmpty(user)) {
                redisUtils.set(Enums.CommonEnum.TOKEN_NAME_SPACE + state, JSONObject.toJSONString(user));
                return ReturnResultUtils.returnSuccess(Enums.CommonEnum.BIND_SUCCESS);
            }
            return ReturnResultUtils.returnFail(Enums.CommonEnum.BIND_ERROR);
        }
        return ReturnResultUtils.returnFail(Enums.CommonEnum.PHONE_AND_CODE_NOT_NULL);
    }

    @ApiOperation(value = "登录")
    @PostMapping(value = "login")
    public ReturnResult<UserLoginVo> login(@Valid UserLoginVo userLoginVo, HttpServletRequest request) throws Exception {
        User user = userService.login(userLoginVo);
        if (!ObjectUtils.isEmpty(user)) {
            String token = request.getSession().getId();
            String userStr = JSONObject.toJSONString(user);
            redisUtils.set(Enums.CommonEnum.TOKEN_NAME_SPACE + token, userStr, 60 * 30);
            return ReturnResultUtils.returnSuccess(token);
        }
        return ReturnResultUtils.returnFail(Enums.CommonEnum.LOGIN_ERROR);
    }


    @ApiOperation(value = "注册")
    @PostMapping(value = "/register")
    public ReturnResult<String> register(@Valid UserResignVo userResignVo) throws Exception {
        if (!ObjectUtils.isEmpty(userResignVo)) {
            Object code = redisUtils.get(Enums.CommonEnum.CODE_NAME_SPACE + userResignVo.getuPhone());
            if (!ObjectUtils.isEmpty(code) && code.toString().equals(userResignVo.getCheck())) {
                User user = userService.register(userResignVo);
                if (!ObjectUtils.isEmpty(user)) {
                    return ReturnResultUtils.returnSuccess(Enums.CommonEnum.REGISTER_SUCCESS);
                }
            }
        }
        return ReturnResultUtils.returnFail(Enums.CommonEnum.REGISTER_FAIL);
    }

    @PostMapping(value = "/sendRegister")
    @ApiOperation(value = "发送手机验证码（注册用）")
    public ReturnResult<String> sendRegister(@ApiParam(value = "手机号", required = true) @RequestParam String phone) {
        if (!ObjectUtils.isEmpty(userService.queryUserByPhone(phone))) {
            return ReturnResultUtils.returnFail(Enums.CommonEnum.PHONE_REPEAT);
        }
        // 60秒内只能发送一次
        if (redisUtils.checkSpeed(Enums.CommonEnum.CHECK_SPEED_NAME_SPACE + phone, 1, 60)) {
            // 随机生成6位验证码发送
            String code = Sms.getRandom().toString();
            redisUtils.set(Enums.CommonEnum.CODE_NAME_SPACE + phone, code, 30 * 60);
            Sms.sendSms(phone, "{code:" + code + "}", "SMS_180960940");
            return ReturnResultUtils.returnSuccess(Enums.CommonEnum.SEND_CODE_SUCCESS);
        }
        return ReturnResultUtils.returnFail(Enums.CommonEnum.CODE_SEND_ERROR);
    }


    @Transactional
    @UserLoginRequired
    @PostMapping(value = "/addShopCartAfterLogin")
    @ApiOperation(value = "登录后将未登录状态的购物车信息加入数据库")
    public void addShopCartLogin(@CurrentUser User user, HttpServletRequest request) {
        String ip = IpUtils.getIpAddress(request);
        Set<String> keysSet = redisUtils.keys(ip + "*");
        keysSet.forEach(key -> {
            String gId = key.substring(key.indexOf(":") + 1);
            int goodsNum = (int) redisUtils.get(key);
            redisUtils.del(key);
            String uid = user.getuId();
            ShopCart shopCart = ShopCart.builder().uId(uid).gId(gId).gCount(goodsNum).build();
            shopCartService.addShopCartLogin(shopCart);
        });

    }
}
