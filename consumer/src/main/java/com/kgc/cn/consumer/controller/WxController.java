package com.kgc.cn.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.kgc.cn.common.enums.Enums;
import com.kgc.cn.common.enums.RedisEnums;
import com.kgc.cn.common.model.Order;
import com.kgc.cn.common.model.User;
import com.kgc.cn.common.service.GoodsService;
import com.kgc.cn.common.service.OrderService;
import com.kgc.cn.common.service.UserService;
import com.kgc.cn.common.utils.client.UrlUtils;
import com.kgc.cn.common.utils.returns.ReturnResult;
import com.kgc.cn.common.utils.returns.ReturnResultUtils;
import com.kgc.cn.consumer.config.aop.UserLoginRequired;
import com.kgc.cn.consumer.utils.WxUtils;
import com.kgc.cn.consumer.utils.activemq.ActiveMqUtils;
import com.kgc.cn.consumer.utils.redis.RedisUtils;
import com.kgc.cn.consumer.utils.wx.WxPayDto;
import com.kgc.cn.consumer.utils.wx.WxPayUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;


/**
 * Created by boot on 2019/12/2.
 */
@Api(tags = "微信", hidden = true)
@RestController
@RequestMapping(value = "/wx")
public class WxController {
    @Autowired
    private WxUtils wxUtils;
    @Autowired
    private RedisUtils redisUtils;
    @Reference
    private UserService userService;
    @Autowired
    private WxPayDto wxPayDto;
    @Reference
    private GoodsService goodsService;
    @Autowired
    private ActiveMqUtils activeMqUtils;
    @Reference
    private OrderService orderService;

    @ApiOperation(value = "test")
    @UserLoginRequired
    @GetMapping(value = "/test")
    public void test() {
        System.out.println(1);
    }

    @GetMapping(value = "/toLogin")
    public String toLogin(HttpServletRequest request) throws Exception {
        return wxUtils.reqCode(request.getSession().getId());
    }

    @GetMapping("/callBack")
    @Transactional
    public ReturnResult<String> callBack(String code, String state) throws Exception {

        // 通过返回的code换取accessToken与openId
        JSONObject jsonObject = JSONObject.parseObject(UrlUtils.loadURL(wxUtils.reqAccessToken(code)));
        String accessToken = jsonObject.getString("access_token");
        String openId = jsonObject.getString("openid");
        User user = userService.checkWxLogin(openId);

        // 通过openid尝试获取用户对象
        if (!ObjectUtils.isEmpty(user)) {
            redisUtils.set(Enums.CommonEnum.TOKEN_NAME_SPACE + state, JSONObject.toJSONString(user));
            return ReturnResultUtils.returnSuccess(Enums.CommonEnum.LOGIN_SUCCESS);
        }
        // 获取失败，通过access_token和openid获取用户信息，要求用户绑定手机号
        String userInfoJsonStr = UrlUtils.loadURL(wxUtils.reqUserInfo(accessToken, openId));
        redisUtils.set(Enums.CommonEnum.OPEN_ID_NAME_SPACE + openId, userInfoJsonStr, 86400);
        redisUtils.set(Enums.CommonEnum.STATE_NAME_SPACE + state, openId, 86400);
        return ReturnResultUtils.returnFail(Enums.CommonEnum.NEED_BIND_PHONE);
    }

    @ApiOperation(value = "返回")
    @RequestMapping("/notify")
    @Transactional
    public String WxPayNotify(HttpServletRequest request, HttpServletResponse response) throws Exception {
        InputStream inputStream = null;
        BufferedReader reader = null;
        inputStream = request.getInputStream();
        reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
        StringBuffer sbf = new StringBuffer();
        String line;
        while ((line = reader.readLine()) != null) {
            sbf.append(line);
        }
        reader.close();
        inputStream.close();
        Map<String, String> map = WxPayUtils.xmlToMap(sbf.toString());
        boolean isSign = WxPayUtils.isSign(map, wxPayDto.getKey());
        if (isSign) {
            //todo 支付成功
            String oid = map.get("out_trade_no");
            if (redisUtils.checkSpeed(oid, 1, 90000)) {
                redisUtils.del(RedisEnums.ORDER_COMMON + oid);
                redisUtils.del(RedisEnums.RUSH_ORDER + oid);
                goodsService.getGoodsAndCounts(oid).forEach(goodsAndCount -> {
                    goodsService.updateSales(goodsAndCount.getgId(), goodsAndCount.getoCount());
                    goodsService.updateStock(goodsAndCount.getgId(), goodsAndCount.getoCount());
                });
                goodsService.updateStatus(oid, 1);
                Order order = orderService.queryOrderById(oid);
                activeMqUtils.sendMsgByQueue("orderSuccess", order);
            }
        }
        return "<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>";
    }


}
