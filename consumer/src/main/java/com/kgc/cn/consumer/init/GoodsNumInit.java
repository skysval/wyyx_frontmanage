package com.kgc.cn.consumer.init;

import com.alibaba.dubbo.config.annotation.Reference;
import com.kgc.cn.common.enums.RedisEnums;
import com.kgc.cn.common.model.Goods;
import com.kgc.cn.common.service.GoodsService;
import com.kgc.cn.consumer.utils.redis.RedisUtils;
import com.sun.org.apache.regexp.internal.RE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Configuration
public class GoodsNumInit implements ApplicationRunner {

    @Reference
    private GoodsService goodsService;

    @Autowired
    private RedisUtils redisUtils;


    @Override
    @Transactional
    public void run(ApplicationArguments applicationArguments) throws Exception {
        List<Goods> goodsList = goodsService.goodsNum();
        goodsList.forEach(goods -> {
            redisUtils.set(RedisEnums.STOCK_NAMESPACE + goods.getgId(), goods.getgStock());
        });


    }
}
