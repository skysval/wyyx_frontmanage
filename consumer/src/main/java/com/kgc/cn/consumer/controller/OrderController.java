package com.kgc.cn.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.kgc.cn.common.enums.Enums;
import com.kgc.cn.common.enums.RedisEnums;
import com.kgc.cn.common.model.Order;
import com.kgc.cn.common.model.User;
import com.kgc.cn.common.service.GoodsService;
import com.kgc.cn.common.service.OrderService;
import com.kgc.cn.common.service.UserService;
import com.kgc.cn.common.utils.id.IdUtils;
import com.kgc.cn.common.utils.returns.ReturnResult;
import com.kgc.cn.common.utils.returns.ReturnResultUtils;
import com.kgc.cn.consumer.config.aop.CurrentUser;
import com.kgc.cn.consumer.config.aop.UserLoginRequired;
import com.kgc.cn.consumer.utils.activemq.ActiveMqUtils;
import com.kgc.cn.consumer.utils.activemq.model.RushOrder;
import com.kgc.cn.consumer.utils.redis.RedisUtils;
import com.kgc.cn.consumer.utils.stock.StockUtils;
import com.kgc.cn.consumer.utils.wx.WxPay;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @Date 2019/12/23 14:05
 * @Creat by Crane
 */

@RestController
@Api(tags = "下单")
@RequestMapping("/order")
public class OrderController {
    @Reference
    private OrderService orderService;

    @Reference
    private UserService userService;

    @Reference
    private GoodsService goodsService;

    @Autowired
    private ActiveMqUtils activeMqUtils;

    @Autowired
    private WxPay wxPay;

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private StockUtils stockUtils;


    @UserLoginRequired
    @PostMapping("/buy")
    @ApiOperation(value = "买")
    public ReturnResult buy(@CurrentUser User user,
                            @ApiParam(value = "商品id", required = true) @RequestParam String gid,
                            @ApiParam(value = "商品数量", required = true) @RequestParam int count) throws Exception {
        Order order = orderService.queryOrder(user.getuId(), gid, count);
        if (!ObjectUtils.isEmpty(order)) {
            // 设置订单未支付的话1天后失效
            redisUtils.set(RedisEnums.ORDER_COMMON + order.getoId(), 1, 86400);
            redisUtils.decr(RedisEnums.STOCK_NAMESPACE + gid, count);
            Map<String, String> map = wxPay.codeUrl(order);
            return ReturnResultUtils.returnSuccess(map);
        }
        return ReturnResultUtils.returnFail(Enums.OrderEnum.ORDER_ADD_FAIL);
    }

    @UserLoginRequired
    @PostMapping("/cleanShopCart")
    @ApiOperation(value = "买买买")
    public ReturnResult payAll(@CurrentUser User user,
                               @ApiParam(value = "商品id，多个id用逗号分割", required = true) @RequestParam String gid) throws Exception {
        Map<String, Integer> countmap = userService.selCount(gid, user.getuId());
        String oid = IdUtils.queryOid();
        stockUtils.decrStock(countmap);
        Map<String, String> map = wxPay.codeUrl(userService.queryOrders(user.getuId(), countmap, oid));
        return ReturnResultUtils.returnSuccess(map);
    }

    @Transactional
    @UserLoginRequired
    @ApiOperation(value = "抢购")
    @GetMapping(value = "/rushBuy")
    public ReturnResult rushBuy(@ApiParam(value = "商品id", required = true) @RequestParam String gid,
                                @CurrentUser User user) throws Exception {
        while (true) {
            if (redisUtils.lock(RedisEnums.ORDER_RUSH, "lock", 1)) {
                if ((int) redisUtils.get(RedisEnums.STOCK_NAMESPACE + gid) <= 0) {
                    return ReturnResultUtils.returnFail(Enums.OrderEnum.STOCK_FAIL);
                } else {
                    if (redisUtils.checkSpeed(user.getuId() + gid, 1, 60 * 60)) {
                        redisUtils.decr(RedisEnums.STOCK_NAMESPACE + gid, 1);
                        redisUtils.delLock(RedisEnums.ORDER_RUSH);
                        Order order = orderService.rushOrder(user.getuId(), gid);
                        redisUtils.set(RedisEnums.RUSH_ORDER + order.getoId(), order, 60);
                        RushOrder rushOrder = RushOrder.builder().oId(order.getoId())
                                .uId(order.getuId()).oPrice(order.getoPrice()).gid(gid).build();
                        activeMqUtils.sendMsgByQueue(RedisEnums.RUSH_ORDER, rushOrder);
                        Map<String, String> map = wxPay.codeUrl(order);
                        return ReturnResultUtils.returnSuccess(map);
                    }
                    return ReturnResultUtils.returnFail(Enums.OrderEnum.ONLY_ONCE);
                }

            }
        }
    }
}
