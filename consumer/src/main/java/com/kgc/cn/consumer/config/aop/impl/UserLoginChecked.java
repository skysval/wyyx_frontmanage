package com.kgc.cn.consumer.config.aop.impl;

import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import com.kgc.cn.common.enums.Enums;
import com.kgc.cn.common.service.UserService;
import com.kgc.cn.consumer.config.aop.UserLoginRequired;
import com.kgc.cn.consumer.utils.WxUtils;
import com.kgc.cn.consumer.utils.redis.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

public class UserLoginChecked implements HandlerInterceptor {

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private WxUtils wxUtils;

    @Reference
    private UserService userService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();

        UserLoginRequired userLoginRequired = method.getAnnotation(UserLoginRequired.class);
        if (userLoginRequired != null) {
            String token = request.getHeader("user_token");
            if (StringUtils.isNotEmpty(token)) {
                String userJsonStr = (String) redisUtils.get(Enums.CommonEnum.TOKEN_NAME_SPACE + token);
                if (StringUtils.isNotEmpty(userJsonStr)) {
                    request.setAttribute("userJsonString", userJsonStr);
                    return true;
                }
            }
            response.sendRedirect("/wx/toLogin");
            return false;
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
