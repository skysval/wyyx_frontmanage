package com.kgc.cn.consumer.utils;

import com.alibaba.fastjson.JSONObject;

public class UserUtils {

    public static String getNickName(Object object) {
        return JSONObject.parseObject(object.toString()).getString("nickname");
    }
}
