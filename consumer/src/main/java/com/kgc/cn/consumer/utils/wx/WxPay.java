package com.kgc.cn.consumer.utils.wx;

import com.kgc.cn.common.model.Order;
import com.kgc.cn.common.utils.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @Date 2019/12/10 15:40
 * @Creat by Crane
 */
@Component
public class WxPay {
    @Autowired
    private WxPayDto wxPayDto;

    public Map<String, String> codeUrl(Order order) throws Exception {
        Map<String, String> param = new HashMap();
        param.put("appid", wxPayDto.getAppid());
        param.put("mch_id", wxPayDto.getMchid());
        param.put("nonce_str", CommonUtil.generateUUID());
        param.put("body", order.getoId());
        param.put("out_trade_no", order.getoId());
        param.put("total_fee", "1"/*String.valueOf(order.getoPrice())*/);
        param.put("spbill_create_ip", "192.168.1.116");
        param.put("notify_url", wxPayDto.getNotifyUrl());
        param.put("time_start", WxPayUtils.getStartTime());
        param.put("time_expire", WxPayUtils.getExpireTime());
        param.put("trade_type", "NATIVE");
        String sign = WxPayUtils.createSignature(param, wxPayDto.getKey());
        param.put("sign", sign);
        String payXml = WxPayUtils.mapToXml(param);
        String resultXml = DoPost.doPost(wxPayDto.getUnifiedorder(), payXml, 5000);
        Map<String, String> resultMap = WxPayUtils.xmlToMap(resultXml);
        return resultMap;
    }

}
