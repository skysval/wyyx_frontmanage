package com.kgc.cn.consumer.listener;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.kgc.cn.common.enums.RedisEnums;
import com.kgc.cn.common.model.Order;
import com.kgc.cn.common.service.OrderService;
import com.kgc.cn.common.service.UserService;
import com.kgc.cn.common.vo.Msg;
import com.kgc.cn.consumer.utils.Sms;
import com.kgc.cn.consumer.utils.activemq.model.RushOrder;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

/**
 * @Date 2019/12/24 20:14
 * @Creat by Crane
 */
@Component
public class SmsListener {

    @Reference
    private UserService userService;

    @Reference
    private OrderService orderService;

    @JmsListener(destination = "orderSuccess")
    public void sendMsg(Order order) {
        String phone = userService.queryUserById(order.getuId()).getuPhone();
        Msg msg = Msg.builder().order(order.getoId()).price(order.getoPrice()).build();
        Sms.sendSms(phone, JSONObject.toJSONString(msg), "SMS_181200377");
    }


    @JmsListener(destination = RedisEnums.RUSH_ORDER)
    public void rushOrder(RushOrder rushOrder) {
        Order order = new Order(rushOrder.getOId(), rushOrder.getUId(), rushOrder.getOPrice(), 0);
        orderService.addRushOrder(order, rushOrder.getGid());
    }
}
