package com.kgc.cn.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.kgc.cn.common.enums.Enums;
import com.kgc.cn.common.service.GoodsService;
import com.kgc.cn.common.utils.returns.ReturnResult;
import com.kgc.cn.common.utils.returns.ReturnResultUtils;
import com.kgc.cn.common.vo.GoodsQueryVo;
import com.kgc.cn.common.vo.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@Api(tags = "商品操作")
@RequestMapping(value = "/user")
public class GoodsController {

    @Reference
    private GoodsService goodsService;

    @ApiOperation(value = "展示商品详情")
    @PostMapping(value = "/showDetail")
    public ReturnResult<GoodsQueryVo> showDetail(@ApiParam(value = "商品id", required = true) @RequestParam String gid) {
        GoodsQueryVo goodsQueryVo = goodsService.showDetails(gid);
        if (goodsQueryVo != null) {
            return ReturnResultUtils.returnSuccess(goodsQueryVo);
        }
        return ReturnResultUtils.returnFail(Enums.GoodsEnum.NONE_ERROR);
    }

    @ApiOperation(value = "展示全部商品")
    @GetMapping(value = "/showAll")
    public ReturnResult<Map<String, Map<String, List<GoodsQueryVo>>>> showAll() {
        return ReturnResultUtils.returnSuccess(goodsService.showAll());
    }

    @ApiOperation(value = "展示总成交额")
    @GetMapping(value = "/showTotalSale")
    public ReturnResult<Long> showTotalSale() {
        return ReturnResultUtils.returnSuccess(goodsService.queryTotalSale());
    }

    @ApiOperation(value = "展示总销量")
    @GetMapping(value = "/showTotalVolume")
    public ReturnResult<Long> showTotalVolume() {
        return ReturnResultUtils.returnSuccess(goodsService.queryTotalVolume());
    }

    @ApiOperation(value = "按类型展示商品")
    @GetMapping(value = "/showGoodsByProperty")
    public ReturnResult<Page> showGoodsByProperty(@ApiParam(value = "类型", required = true) @RequestParam String property,
                                                  @ApiParam(value = "当前页") @RequestParam(defaultValue = "1") int startRow,
                                                  @ApiParam(value = "每页展示数量") @RequestParam(defaultValue = "10") int pageSize) {
        return ReturnResultUtils.returnSuccess(goodsService.queryGoodsByProperty(property, startRow, pageSize));
    }

}
