package com.kgc.cn.consumer.utils.wx;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * @Date 2019/12/10 15:57
 * @Creat by Crane
 */

@Data
@ConfigurationProperties(prefix = "wxPay")
@Component
public class WxPayDto implements Serializable {
    private String appid;
    private String mchid;
    private String notifyUrl;
    private String key;
    private String unifiedorder;
}
